package Ladung;

public class Ladung {
    private String bezeichnung1;
    private int menge1;
    private String bezeichnung2;
    private int menge2;

    public String getBezeichnung1() {
        return bezeichnung1;
    }

    public void setBezeichnung1(String bezeichnung1) {
        this.bezeichnung1 = bezeichnung1;
    }

    public String getBezeichnung2() {
        return bezeichnung2;
    }

    public void setBezeichnung2(String bezeichnung2) {
        this.bezeichnung2 = bezeichnung2;
    }


    public int getMenge1() {
        return menge1;
    }

    public void setMenge1(int menge1) {
        this.menge1 = menge1;
    }

    public int getMenge2() {
        return menge2;
    }

    public void setMenge2(int menge2) {
        this.menge2 = menge2;
    }

    public Ladung(String bezeichnung1, int menge1, String bezeichnung2, int menge2) {
        this.bezeichnung1 = bezeichnung1;
        this.bezeichnung2 = bezeichnung2;
        this.menge1 = menge1;
        this.menge2 = menge2;
    }

    @Override
    public String toString() {
        return "Ladung: " +
                " bezeichnung = " + bezeichnung1 +
                ", menge = " + menge1
                ;
    }
}