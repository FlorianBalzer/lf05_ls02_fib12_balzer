import Ladung.Ladung;
import Raumschiff.Raumschiff;

/**
 * @author Florian Balzer
 */

public class Test {
    public static void main(String[] args) {
        Raumschiff Klingonen = new Raumschiff("IKS Hegh'ta", 1, 100.0, 100.0, 100.0, 100.0, 2, "Unbekanntes Log");
        Raumschiff Romulaner = new Raumschiff("IRW Khazara", 2, 100.0, 100.0, 100.0, 100.0, 2, "Unbekannts Log");
        Raumschiff Vulkanier = new Raumschiff("Ni'Var", 0, 80.0, 80.0, 50.0, 100.0, 5, "Unbekanntes Log");
        System.out.println("Klingonen = " + Klingonen);
        System.out.println("Romulaner = " + Romulaner);
        System.out.println("Vulkanier = " + Vulkanier);

        Ladung l1 = new Ladung("Ferengi Schneckensaft",200,"leer",0);
        Ladung l2 = new Ladung("Borg-Schrott",5, "Rote Materie", 2);
        Ladung l3 = new Ladung("Forschungssonde", 35, "leer", 0);
        System.out.println("l1 = " + l1);
        System.out.println("l2 = " + l2);
        System.out.println("l3 = " + l3);
    }
}
