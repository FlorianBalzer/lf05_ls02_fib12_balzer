package Raumschiff;

import Ladung.Ladung;

import java.util.ArrayList;

public class Raumschiff {
    private String schiffsname;
    private int photonenTorpedoAnzahl;
    private double energieVersorgungInProzent;
    private double schildeInProzent;
    private double huelleInProzent;
    private double lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private ArrayList<String> broadcastCommunicator = new ArrayList<String>();
    private ArrayList<Ladung> ladung = new ArrayList<Ladung>();

    public ArrayList<Ladung> getLadung() {
        return ladung;
    }

    public void setLadung(ArrayList<Ladung> ladung) {
        this.ladung = ladung;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public int getPhotonenTorpedoAnzahl() {
        return photonenTorpedoAnzahl;
    }

    public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
    }

    public double getEnergieVersorgungInProzent() {
        return energieVersorgungInProzent;
    }

    public void setEnergieVersorgungInProzent(double energieVersorgungInProzent) {
        this.energieVersorgungInProzent = energieVersorgungInProzent;
    }

    public double getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(double schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public double getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(double huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public double getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(double lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public Raumschiff(String schiffsname, int photonenTorpedoAnzahl, double energieVersorgungInProzent, double schildeInProzent, double huelleInProzent, double lebenserhaltungssystemeInProzent, int androidenAnzahl, String broadcastKommunicator) {
        this.schiffsname = schiffsname;
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
        this.energieVersorgungInProzent = energieVersorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
    }

    @Override
    public String toString() {
        return "Raumschiff: " +
                " name = " + schiffsname +
                ", photonenTorpedoAnzahl = " + photonenTorpedoAnzahl +
                ", energieVersorgungInProzent = " + energieVersorgungInProzent +
                ", schildeInProzent = " + schildeInProzent +
                ", huelleInProzent = " + huelleInProzent +
                ", lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent +
                ", androidenAnzahl = " + androidenAnzahl
                ;
    }

    public void zustandRaumschiff() {
        System.out.println("Raumschiff: " +
                " name = " + schiffsname +
                ", photonenTorpedoAnzahl = " + photonenTorpedoAnzahl +
                ", energieVersorgungInProzent = " + energieVersorgungInProzent +
                ", schildeInProzent = " + schildeInProzent +
                ", huelleInProzent = " + huelleInProzent +
                ", lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent +
                ", androidenAnzahl = " + androidenAnzahl);
    }

    public void photonenTorpedosAbschießen(int photonenTorpedoAnzahl) {
        if (photonenTorpedoAnzahl == 0) {
            System.out.println("-=*Click*=-");
        } else {
            this.photonenTorpedoAnzahl = photonenTorpedoAnzahl - 1;
            System.out.println("Photonentorpedo abgeschossen");
            treffer();
        }
    }

    public void phaserkanoneAbschießen() {
        if (this.energieVersorgungInProzent < 50) {
            System.out.println("-=*Click*=-");
        } else {
            System.out.println("Phaserkanone abgeschossen");
            treffer();
        }
    }

    public void treffer() {
        System.out.println(schiffsname + "wurde getroffen");
    }

    public void addLadung(Ladung ladung) {
        ladung = new Ladung("Unerforscht", 0, "leer", 0);
        ArrayList<Ladung> ladungArrayList = new ArrayList<>();
        ladungArrayList.add(ladung);
    }

    public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
        this.broadcastCommunicator = broadcastCommunicator;
    }

    public ArrayList<String> getBroadcastCommunicator() {
        return broadcastCommunicator;
    }

    public void abschussTorpedos() {
        if (photonenTorpedoAnzahl == 0) {
            System.out.println("-=*Click*=-");
        } else {
            photonenTorpedoAnzahl = photonenTorpedoAnzahl - 1;
            System.out.println("Photonen Torpedos abgeschossen");
        }
    }
}
